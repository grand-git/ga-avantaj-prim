package org.example;

import com.codeborne.selenide.Configuration;
import com.opencsv.CSVWriter;
import org.apache.log4j.Logger;
import org.example.web.BoardPage;
import org.junit.Test;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.codeborne.selenide.Selenide.open;
import static org.junit.Assert.assertTrue;

public class FilterChecking {

    private final static Logger log = Logger.getLogger(FilterChecking.class);
    private static final int RESULT_QUANTITY = 101;
    private final static char CSV_SEPARATOR = '|';
    private final BoardPage boardPage = new BoardPage();
    private final BoardPageService boardPageService = new BoardPageService();

    @Test
    public void checkFiltersApplyingRu() {
        String url = "https://999.md/ru/list/transport/cars";
        String outputCsvFile = "filter-result-ru.csv";

        ArrayList<Filter> setOfAppliedFilters = new ArrayList<>();
        // Раздел, категория и подкатегория брать из части фильтра из UI. Фильтр устанавливается в следующем формате
        // - (Раздел, категория)
        // - (Раздел, категория, подкатегория)
        // - (Раздел, от, до)
        setOfAppliedFilters.add(new Filter("По маркам", "BMW"));
        setOfAppliedFilters.add(new Filter("Год выпуска", 2015, 2021));
        setOfAppliedFilters.add(new Filter("Комфорт", "Кондиционер"));

        ArrayList<Filter> setOfCheckedFilters = new ArrayList<>();
        // Название фильтра и значение нужно брать со страницы объявления из UI. Фильтр устанавливается в следующем формате
        // - (Название фильтра (без значения))
        // - (Название фильтра, значение)
        setOfCheckedFilters.add(new Filter("Марка", "BMW"));
        setOfCheckedFilters.add(new Filter("Кондиционер"));

        // Название фильтра(включая по-умолчанию) нужно брать со страницы объявления из UI. Названия разделаются установленным разделителем
        String[] columnsForCsv = new String[]{
                DefaultAdParameters.link.parameter,
                DefaultAdParameters.title.parameter,
                "Цена:",
                DefaultAdParameters.currency.parameter,
                "Год выпуска"
        };

        checkFiltersApplying(url, setOfAppliedFilters, setOfCheckedFilters, columnsForCsv, outputCsvFile);
    }

    @Test
    public void checkFiltersApplyingRo() {
        String url = "https://999.md/ro/list/transport/cars";
        String outputCsvFile = "filter-result-ro.csv";

        ArrayList<Filter> setOfAppliedFilters = new ArrayList<>();
        setOfAppliedFilters.add(new Filter("Marcă", "BMW"));
        setOfAppliedFilters.add(new Filter("An de fabricație", 2015, 2021));
        setOfAppliedFilters.add(new Filter("Confort", "Aer condiționat"));

        ArrayList<Filter> setOfCheckedFilters = new ArrayList<>();
        setOfCheckedFilters.add(new Filter("Marcă", "BMW"));
        setOfCheckedFilters.add(new Filter("Aer condiționat"));

        String[] columnsForCsv = new String[]{
                DefaultAdParameters.link.parameter,
                DefaultAdParameters.title.parameter,
                "Preț:",
                DefaultAdParameters.currency.parameter,
                "An de fabricație"
        };

        checkFiltersApplying(url, setOfAppliedFilters, setOfCheckedFilters, columnsForCsv, outputCsvFile);
    }

    private void checkFiltersApplying(String url, ArrayList<Filter> setOfAppliedFilters, ArrayList<Filter> setOfCheckedFilters,
                                      String[] columnsForCsv, String outputCsvFile) {
        Configuration.headless = true;
        openPage(url);
        applyFilters(setOfAppliedFilters);
        List<ResultRow> filterResultsList = boardPageService.getResults(setOfCheckedFilters, RESULT_QUANTITY);
        saveResultsIntoCsv(outputCsvFile, columnsForCsv, filterResultsList);
    }

    private void openPage(String url) {
        log.info("Opening page [" + url + "] ...");
        open(url);
        assertTrue(boardPage.isAt());
        log.info("Page is opened");
    }

    private void applyFilters(ArrayList<Filter> setOfFilters) {
        log.info("Applying filters...");
        assertTrue(boardPage.applyFilters(setOfFilters));
        log.info("Filters are applied");
    }

    private void saveResultsIntoCsv(String fileName, String[] tagsToCsv, List<ResultRow> resultsList) {
        log.info("Save results into CSV [" + fileName + "]...");
        List<String[]> csvData = new ArrayList<>();
        csvData.add(tagsToCsv);
        csvData.addAll(prepareCsvData(tagsToCsv, resultsList));
        try (FileWriter writer = new FileWriter(fileName)) {
            CSVWriter cswWriter = new CSVWriter(writer, CSV_SEPARATOR, CSVWriter.NO_QUOTE_CHARACTER, CSVWriter.DEFAULT_ESCAPE_CHARACTER, CSVWriter.RFC4180_LINE_END);
            cswWriter.writeAll(csvData);
        } catch (IOException e) {
            log.info("Output CSV file can't be created");
            e.printStackTrace();
        }
        log.info("Data are saved into CSV file");
    }

    private List<String[]> prepareCsvData(String[] tagsToCsv, List<ResultRow> resultsList) {
        List<String[]> csvData = new ArrayList<>();
        resultsList.forEach(x -> {
            List<String> row = new ArrayList<>();
            for (String tag : tagsToCsv) {
                row.add(x.getParameterValue(tag));
            }
            csvData.add(row.toArray(new String[0]));
        });
        return csvData;
    }

}
