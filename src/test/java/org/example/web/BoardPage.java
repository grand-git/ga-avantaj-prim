package org.example.web;

import com.codeborne.selenide.SelenideElement;
import lombok.Getter;
import lombok.experimental.Accessors;
import org.apache.log4j.Logger;
import org.example.Filter;

import java.util.ArrayList;
import java.util.List;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$$x;
import static com.codeborne.selenide.Selenide.$x;

@Getter
@Accessors(fluent = true)
public class BoardPage {

    private final static Logger log = Logger.getLogger(BoardPage.class);
    private final String itemsList = "//*[@class='items__list__container']";
    private final SelenideElement filtersForm = $x("//*[@id='js-ads-filters']");
    private final String sectionLabel = ".//parent::label";
    private final String shawAll = ".//label[@class='items__filters__filter__content__show-more']";
    private final String categoryList = ".//following-sibling::div[@class='items__filters__filter__content js-filter-content']";
    private final String categoryCheckbox = "input";
    private final String subcategoryCheckbox = "input";
    private final String from = ".//following-sibling::div/div/div/input[@class=' js-numeric form-element__input ']";
    private final String to = ".//following-sibling::div/div/div/input[@class=' form-element__input ']";
    private final String unit = ".//following-sibling::div/div/div/select[@class=' form-element__select has-autowidth ']";
    private final List<SelenideElement> adsList = $$x("//li[contains(@class, 'ads-list-photo-item')]/div[@class='ads-list-photo-item-title ']/a");
    private final String boosterLabel = ".//span[contains(@class,'booster-label')]";
    private final SelenideElement nextPageNumber = $x("//li[@class='current']/following-sibling::li/a");

    public boolean isAt() {
        return $x(itemsList).shouldBe(visible).is(visible);
    }

    // TODO: improve. when filter is updated page is not accessible. find better method to check if page is updated
    private void refreshPage() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public boolean applyFilters(ArrayList<Filter> setOfFilters) {
        for (Filter filter : setOfFilters) {
            SelenideElement section = openSection(filter.getSection()).parent();
            if (!filter.getCategory().isEmpty()) {
                if (!filter.getSubcategory().isEmpty()) {
                    openCategory(filter.getSection(), filter.getCategory());
                    openSubcategory(filter.getSection(), filter.getSubcategory());
                } else {
                    chooseCategory(section, filter.getCategory());
                }
            }
            if (filter.getFrom() != 0) {
                section = findSection(filter.getSection());
                section.$x(from).shouldBe(visible).scrollTo().setValue(String.valueOf(filter.getFrom()));
                refreshPage();
                log.info("Field 'from' for section [" + filter.getSection() + "] is set to [" + filter.getFrom() + "]");
            }
            if (filter.getTo() != 0) {
                section = findSection(filter.getSection());
                section.$x(to).scrollTo().setValue(String.valueOf(filter.getTo()));
                refreshPage();
                log.info("Field 'to' for section [" + filter.getSection() + "] is set to [" + filter.getTo() + "]");
            }
            if (!filter.getDropDownValue().isEmpty()) {
                section = findSection(filter.getSection());
                section.$x(unit).scrollTo().selectOption(filter.getDropDownValue());
                refreshPage();
                log.info("Field 'dropdown' for section [" + filter.getSection() + "] is set to [" + filter.getDropDownValue() + "]");
            }
        }
        return true;
    }

    private SelenideElement openSection(String sectionName) {
        log.info("Opening section [" + sectionName + "] ...");
        SelenideElement section = findSection(sectionName);
        if (!section.$x(categoryList).exists()) {
            section.click();
            refreshPage();
            section = findSection(sectionName);
        }
        if (section.parent().$x(shawAll).exists()) {
            section.parent().$x(shawAll).click();
            refreshPage();
            section = findSection(sectionName);
        }
        log.info("Section [" + sectionName + "] is opened");
        return section;
    }

    private SelenideElement findSection(String sectionName) {
        return findElementByText(filtersForm, sectionName).$x(sectionLabel);
    }

    private void openCategory(String sectionName, String categoryName) {
        log.info("Opening category [" + categoryName + "] ...");
        SelenideElement category = findCategory(sectionName, categoryName);
        category.click();
        refreshPage();
        findSection(sectionName);
        findCategory(sectionName, categoryName);
        log.info("Category [" + categoryName + "] is opened");
    }

    private SelenideElement findCategory(String sectionName, String categoryName) {
        SelenideElement section = findSection(sectionName);
        return findElementByText(section.parent(), categoryName);
    }

    private void chooseCategory(SelenideElement node, String categoryName) {
        log.info("Choosing category [" + categoryName + "] ...");
        SelenideElement category = findElementByText(node, categoryName).parent();
        category.$x(categoryCheckbox).click();
        refreshPage();
        log.info("Category [" + categoryName + "] is chosen");
    }

    private void openSubcategory(String sectionName, String subcategoryName) {
        log.info("Opening subcategory [" + subcategoryName + "] ...");
        SelenideElement subcategory = findElementByText(findSection(sectionName).$x(categoryList), subcategoryName).parent();
        subcategory.$x(subcategoryCheckbox).click();
        refreshPage();
        log.info("Subcategory [" + subcategoryName + "] is opened");
    }

    private SelenideElement findElementByText(SelenideElement node, String elementText) {
        log.info("Searching element by text [" + elementText + "] ...");
        return node.$$(byText(elementText)).stream()
                .filter(x -> x.getText().equals(elementText))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("Element with text [" + elementText + "] is missing on page"));
    }

}
