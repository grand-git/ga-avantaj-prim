package org.example.web;

import com.codeborne.selenide.SelenideElement;
import lombok.Getter;
import lombok.experimental.Accessors;

import java.util.List;

import static com.codeborne.selenide.Selenide.$$x;
import static com.codeborne.selenide.Selenide.$x;

@Getter
@Accessors(fluent = true)
public class AdPage {

    private final SelenideElement title = $x("//h1[@itemprop='name']");
    private final SelenideElement content = $x("//div[contains(@class,'adPage__content__description')]");
    private final SelenideElement priceTag = $x("//div[contains(@class,'adPage__content__price-feature__title')]");
    private final SelenideElement priceValue = $x("//span[contains(@class,'adPage__content__price-feature__prices__price__value')]");
    private final SelenideElement priceCurrency = $x("//span[contains(@class,'adPage__content__price-feature__prices__price__currency')]");
    private final List<SelenideElement> parametersWithValue = $$x("//li[@class='m-value']");
    private final String paramName = ".//span[@class=' adPage__content__features__key  ']";
    private final String paramValue = ".//span[@class=' adPage__content__features__value   ']";
    private final List<SelenideElement> parametersWithoutValue = $$x("//li[@class='m-no_value']");

}
