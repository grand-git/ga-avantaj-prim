package org.example;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.apache.log4j.Logger;
import org.example.web.AdPage;
import org.example.web.BoardPage;
import org.openqa.selenium.Keys;

import java.util.*;

import static com.codeborne.selenide.Selenide.*;

public class BoardPageService {

    private final static Logger log = Logger.getLogger(FilterChecking.class);
    private final BoardPage boardPage = new BoardPage();

    public List<ResultRow> getResults(ArrayList<Filter> setOfFilters, int rowQuantity) {
        log.info("Getting results ...");
        List<ResultRow> resultList = new ArrayList<>();
        while (rowQuantity != 0) {
            for (SelenideElement ad : boardPage.adsList()) {
                if (!Objects.requireNonNull(ad.getAttribute("href")).contains("booster")) {
                    log.info("[" + ad + "] is getting...");
                    ResultRow item = new ResultRow(ad.getText());
                    item.addParameters(Collections.singletonMap(DefaultAdParameters.link.parameter, ad.getAttribute("href")));
                    String selectLinkOpenInNewTab = Keys.chord(Keys.CONTROL, Keys.RETURN);
                    ad.sendKeys(selectLinkOpenInNewTab);
                    switchTo().window(1);
                    Map<String, String> parameters = getAllParameters();
                    if (!parameters.isEmpty()) {
                        item.addParameters(parameters);
                        if (isAllFiltersArePresented(setOfFilters, item)) {
                            resultList.add(item);
                            if (rowQuantity != 0) {
                                log.info(rowQuantity + " ads are remained to add");
                                rowQuantity--;
                            } else {
                                break;
                            }
                        }
                    }
                    closeWindow();
                    switchTo().window(0);
                }
            }
            if (rowQuantity != 0) {
                if (boardPage.nextPageNumber().exists()) {
                    log.info("Move to the next page of results");
                    boardPage.nextPageNumber().click();
                    refresh();
                } else {
                    throw new RuntimeException("There is no more ads matching current filter");
                }
            }
        }
        log.info("Results are got");
        return resultList;
    }

    private Map<String, String> getAllParameters() {
        AdPage adPage = new AdPage();
        Map<String, String> parameters = new HashMap<>();
        if (adPage.title().shouldBe(Condition.exist).exists()) {
            parameters.put(DefaultAdParameters.title.parameter, adPage.title().getText());
            parameters.put(adPage.priceTag().getText(), adPage.priceValue().getText());
            parameters.put(DefaultAdParameters.currency.parameter, adPage.priceCurrency().exists() ? adPage.priceCurrency().getText() : "");
            adPage.parametersWithValue().forEach(parameter -> parameters.put(parameter.$x(adPage.paramName()).getText(), parameter.$x(adPage.paramValue()).getText()));
            adPage.parametersWithoutValue().forEach(parameter -> parameters.put(parameter.$x(adPage.paramName()).getText(), ""));
        } else {
            log.info("There is no ads title");
        }
        return parameters;
    }

    private boolean isAllFiltersArePresented(ArrayList<Filter> setOfFilters, ResultRow item) {
        for (Filter filter : setOfFilters) {
            if (item.getParameterValue(filter.getSection()) == null) {
                return false;
            }
        }
        return true;
    }
}
