package org.example;

public enum DefaultAdParameters {
    link("link"),
    title("title"),
    currency("currency");

    public final String parameter;

    DefaultAdParameters(String parameter) {
        this.parameter = parameter;
    }
}
