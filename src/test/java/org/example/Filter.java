package org.example;

import lombok.Getter;

@Getter
public class Filter {

    private final String section;
    private String category = "";
    private String subcategory = "";
    private int from = 0;
    private int to = 0;
    private String dropDownValue = "";

    public Filter(String section, String category, String subcategory) {
        this.section = section;
        this.category = category;
        this.subcategory = subcategory;
    }

    public Filter(String section, String category) {
        this.section = section;
        this.category = category;
    }

    public Filter(String section) {
        this.section = section;
    }

    public Filter(String section, int from, int to, String dropDownValue) {
        this.section = section;
        this.from = from;
        this.to = to;
        this.dropDownValue = dropDownValue;
    }

    public Filter(String section, int from, int to) {
        this.section = section;
        this.from = from;
        this.to = to;
    }

    @Override
    public String toString() {
        return "Filter{" +
                "section='" + section + '\'' +
                ", category='" + category + '\'' +
                ", subcategory='" + subcategory + '\'' +
                ", from=" + from +
                ", to=" + to +
                ", dropDownValue='" + dropDownValue + '\'' +
                '}';
    }
}
