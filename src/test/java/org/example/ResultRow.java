package org.example;

import java.util.HashMap;
import java.util.Map;

public class ResultRow {

    private final Map<String, String> parameters = new HashMap<>();

    public ResultRow(String link) {
        parameters.put(DefaultAdParameters.link.parameter, link);
    }

    public void addParameters(Map<String, String> parameters) {
        this.parameters.putAll(parameters);
    }

    public String getParameterValue(final String key) {
        return parameters.getOrDefault(key, null);
    }

    @Override
    public String toString() {
        return "ResultRow{" +
                "parameters=" + parameters +
                '}';
    }
}
